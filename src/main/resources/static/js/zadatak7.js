console.log("Hello!");

function Zona(id, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima){
	this.id = id;
	this.naziv = naziv;
	this.cenaZaSat = cenaZaSat;
	this.dozvoljenoVremeParkingaUSatima = dozvoljenoVremeParkingaUSatima;
	this.cenaSaPDV = function(){
		return this.cenaZaSat * 0.18;
	}
}


let formaUnos = document.querySelector("form");
let tabelaIspis = document.querySelectorAll("table")[1];
let zona = [];


function ispis(){
	while(tabelaIspis.rows.length > 1){  //brisem sto je bilo prethodno
        tabelaIspis.rows[1].remove();
    	}
    	
		for (let it of zona) {
			let red = tabelaIspis.insertRow(-1);
			 red.innerHTML = 
				'<tr>' +
				'<td>' + it.naziv + '</td>' + 
				'<td>' + it.cenaZaSat + '</td>' +
				'<td>' + it.cenaSaPDV() + '</td>' +
				'<td>' + it.dozvoljenoVremeParkingaUSatima + '</td>' +
				'</tr>'; 
		}
}

function ucitajPodatke(){
	let params = "?";	//ovde ide parametar ako ima
	
	let zahtev = new XMLHttpRequest();
	zahtev.open("GET", "Zona" + params);
	zahtev.responseType = "json";
	zahtev.onload = function(){
//		console.log(zahtev.response);
		let status = zahtev.response.status;
		let odgovor = zahtev.response.zona;
//		let odgovor = JSON.parse(zahtev.response.zona);
		
		if(status == "ok"){
//			console.log("Usao u if");
			for(let it in odgovor){
//				console.log("id: " + odgovor[it].id);
//				console.log("naziv: " + odgovor[it].naziv);
//				console.log("cenaZaSat: " + odgovor[it].cenaZaSat);
//				console.log("dozvoljenoVremeParkingaUSatima: " + odgovor[it].dozvoljenoVremeParkingaUSatima);
				zona[it] = new Zona(odgovor[it].id, odgovor[it].naziv, odgovor[it].cenaZaSat, odgovor[it].dozvoljenoVremeParkingaUSatima);
			}
			ispis();
		}	
	}
	zahtev.send();
	return false;
}


function unosPodataka(){
	let naziv = formaUnos.querySelectorAll("input[type=text]")[0].value;
	let cenaZaSat = parseFloat(formaUnos.querySelectorAll("input[type=text]")[1].value);
	let dozvoljenoVremeParkingaUSatima = parseInt(formaUnos.querySelectorAll("input[type=text]")[2].value);
	let idiDalje = true;
	
	if(!(naziv != "") && (cenaZaSat != "") && (dozvoljenoVremeParkingaUSatima!= "")){
		alert("Morate popuniti sva polja!");
		idiDalje = false;
	}
	if(isNaN(cenaZaSat) || isNaN(dozvoljenoVremeParkingaUSatima)){
		alert("Zadnja dva polja moraju biti brojevi!");
		idiDalje = false;
	}
	if(idiDalje){
		console.log("Salji!");
//		let novaZona = new Zona(0, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima);
//		let posalji = JSON.stringify(novaZona);
		let posalji = "?naziv=" + naziv + "&cenaZaSat=" +  cenaZaSat.toString() + "&dozvoljenoVremeParkingaUSatima=" + dozvoljenoVremeParkingaUSatima.toString();
		
		let zahtev = new XMLHttpRequest();
		zahtev.open("POST", "Zona/Create");
		zahtev.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		zahtev.responseType = "json";
		zahtev.onload = function(){
			if(zahtev.response.status == "ok"){
				zona = [];
				ucitajPodatke();
				ispis();
			} 
		}
		console.log("posalji: " + posalji);
		zahtev.send(posalji);
//		return false;
	}
}


formaUnos.onsubmit = function(){ 
	unosPodataka();
	return false;
}


ucitajPodatke();

	
