package parking.service;

import java.util.Collection;

import parking.model.Zona;

public interface ZonaService {
	
	public Collection<Zona> findAll();

	public void add(Zona zona);
}
