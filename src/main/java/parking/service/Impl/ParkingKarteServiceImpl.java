package parking.service.Impl;

import java.util.ArrayList;
import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import parking.dao.ParkingKarteDAO;
import parking.model.ParkingKarte;
import parking.service.ParkingKarteService;

@Service
public class ParkingKarteServiceImpl implements ParkingKarteService {

	@Autowired
	private ParkingKarteDAO parkingKarteDAO;
	
	@Override
	public Collection<ParkingKarte> findAll(int trajanjeOd, int trajanjeDo) {
		Collection<ParkingKarte> parkingKarteCollection = parkingKarteDAO.findAll();
		Collection<ParkingKarte> rezultat = new ArrayList<>();
		
		for (ParkingKarte it : parkingKarteCollection) {
			if (!(it.getTrajanjeUMinutima() >= trajanjeOd && it.getTrajanjeUMinutima() <= trajanjeDo)) {
				continue;
			}
			rezultat.add(it);
		}
		return rezultat;
	}

}
