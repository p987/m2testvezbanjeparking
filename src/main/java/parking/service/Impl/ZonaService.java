package parking.service.Impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import parking.dao.ZonaDAO;
import parking.model.Zona;

@Service
public class ZonaService implements parking.service.ZonaService {
	
	@Autowired
	ZonaDAO zonaDAO;
	
	@Override
	public Collection<Zona> findAll() {
		
		return zonaDAO.findAll();
	}

	@Override
	public void add(Zona zona) {
		//provere za zonu
		zonaDAO.add(zona);
	}

}
