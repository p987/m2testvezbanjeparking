package parking.service;

import java.util.Collection;

import parking.model.ParkingKarte;

public interface ParkingKarteService {

	public Collection<ParkingKarte> findAll(int trajanjeOd, int trajanjeDo);

}
