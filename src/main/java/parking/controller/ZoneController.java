package parking.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import parking.model.Zona;
import parking.service.ZonaService;

@RestController
@RequestMapping("/Zona")
public class ZoneController {
	
	@Autowired
	ZonaService zonaService;
	
	@GetMapping
	public Map<String, Object> prikazSvih() {
		
		Collection<Zona> zonaColection = zonaService.findAll();
		System.out.print("Get: ");
		System.out.println(zonaColection);
		
		Map<String, Object> odgovor = new HashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("zona", zonaColection);
		return odgovor;
	}
	
	@PostMapping("/Create")
	public Map<String, Object> prihvatiPodatke(
			@RequestParam String naziv ,
			@RequestParam Double cenaZaSat,
			@RequestParam Integer dozvoljenoVremeParkingaUSatima
			) {
		//	BindingResult rez
		
		try {
		System.out.println("Posiljak:");
		Zona zona = new Zona(0, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima);
		String status = "ok";
		zonaService.add(zona);
		Map<String, Object> odgovor = new HashMap<>();
		odgovor.put("status", status);
		return odgovor;
		} catch (Exception ex){
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešno dodavanje!";
			}
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", poruka);
			return odgovor;
		}
	}
}
