package parking.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import parking.model.ParkingKarte;
import parking.model.Zona;
import parking.service.ParkingKarteService;

@Controller
@RequestMapping(value="/ParkingKarte")
public class ParkingKarteController {
	public static final String ZONA_KEY = "zona";
	public static final String PARKING_KARTE = "parkingKarte";
	public static final String UKUPNO_KREIRANE_KARTE_ZONE_KEY = "crvena";
	public static final String KORISNIK_PARKING_KEY = "korisnik";
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private ParkingKarteService parkingKarteService;
	
	
	
	
	@PostMapping("/Create")
	public String zadatak2(
			@RequestParam String registarskaOznakaVozila,
			@RequestParam String pocetakParkinga,
			@RequestParam int trajanjeUMinutama,
			@RequestParam String osobaSaInvaliditetom,
			@RequestParam int zonaId,
			HttpSession session) throws IOException {
		
		Map<Long, ParkingKarte> parkingKarteMap = (Map<Long, ParkingKarte>) servletContext.getAttribute(PARKING_KARTE);
		Map<Integer, Zona> zonaMap = (Map<Integer, Zona>) servletContext.getAttribute(ZONA_KEY);
		int ukupnoKreiraneKareteZone = (int) servletContext.getAttribute(UKUPNO_KREIRANE_KARTE_ZONE_KEY);
		List<ParkingKarte> korisnikList = (List<ParkingKarte>) session.getAttribute(KORISNIK_PARKING_KEY);
		
		Zona zona = zonaMap.get(zonaId); 
		if (zona.getNaziv().toLowerCase() == UKUPNO_KREIRANE_KARTE_ZONE_KEY) {
			ukupnoKreiraneKareteZone++;
		}
		servletContext.setAttribute(UKUPNO_KREIRANE_KARTE_ZONE_KEY, ukupnoKreiraneKareteZone);
		
		long id = parkingKarteMap.values().size() + 1;
		ParkingKarte parkingKarta = new ParkingKarte(id, registarskaOznakaVozila, pocetakParkinga, trajanjeUMinutama, osobaSaInvaliditetom, zona);
		parkingKarteMap.put(id, parkingKarta);
		
		korisnikList.add(parkingKarta); //zadatak4
		
		//ispis u konzolu
		for (ParkingKarte it : parkingKarteMap.values()) {
			System.out.println(it);
		}
		System.out.println();
		System.out.println("Ukupno kreirane karte za boju " + UKUPNO_KREIRANE_KARTE_ZONE_KEY + ": " + servletContext.getAttribute(UKUPNO_KREIRANE_KARTE_ZONE_KEY));
		
		return "redirect:/ParkingKarte/Zadatak3";
	}
	
	
	
	@GetMapping(value = "/Zadatak3")
	public String prikaz(ModelMap model, HttpSession session) throws IOException {
		Map<Long, ParkingKarte> parkingKarteMap = (Map<Long, ParkingKarte>) servletContext.getAttribute(PARKING_KARTE);
		List<ParkingKarte> parkingKarteList = new ArrayList<>(parkingKarteMap.values());
		List<ParkingKarte> korisnikList = (List<ParkingKarte>) session.getAttribute(KORISNIK_PARKING_KEY);
		
		double ukupnaCena = 0;
		for (ParkingKarte it : parkingKarteList) {
			if (it.getOsobaSaInvaliditetom().toUpperCase().equals("DA")) {
				continue;
			}
			int cas = Math.round(it.getTrajanjeUMinutima()/60);
			ukupnaCena += it.getZona().getCenaZaSat() * cas;
		}
		model.put(PARKING_KARTE, parkingKarteList);
		model.put("ukupnaCena", ukupnaCena);
		return "zadatak3";
	}
	
	
	
	@GetMapping(value = "/Zadatak5")
	public String zadatak5(
			@RequestParam(name = "trajanjeOd", required = false,  defaultValue = "0") String trajanjeOdStr,
			@RequestParam(name = "trajanjeDo", required = false,  defaultValue = "2147483647") String trajanjeDoStr,
			HttpSession session,
			ModelMap model) throws IOException {
		
		int trajanjeOd = Integer.parseInt(trajanjeOdStr);
		int trajanjeDo = Integer.parseInt(trajanjeDoStr);
		Collection<ParkingKarte> parkingKarte =  parkingKarteService.findAll(trajanjeOd, trajanjeDo);
		model.put(PARKING_KARTE, parkingKarte);
		return "zadatak5";
	}
	
	
	
}
