package parking.model;

import java.util.Objects;

public class Zona {
	private int id;
	private String naziv;
	private double cenaZaSat;
	private int dozvoljenoVremeParkingaUSatima;
	
	
	public Zona() {
	}


	public Zona(int id, String naziv, double cenaZaSat, int dozvoljenoVremeParkingaUSatima) {
		this.id = id;
		this.naziv = naziv;
		this.cenaZaSat = cenaZaSat;
		this.dozvoljenoVremeParkingaUSatima = dozvoljenoVremeParkingaUSatima;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public double getCenaZaSat() {
		return cenaZaSat;
	}


	public void setCenaZaSat(double cenaZaSat) {
		this.cenaZaSat = cenaZaSat;
	}


	public int getDozvoljenoVremeParkingaUSatima() {
		return dozvoljenoVremeParkingaUSatima;
	}


	public void setDozvoljenoVremeParkingaUSatima(int dozvoljenoVremeParkingaUSatima) {
		this.dozvoljenoVremeParkingaUSatima = dozvoljenoVremeParkingaUSatima;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zona other = (Zona) obj;
		return id == other.id;
	}


	@Override
	public String toString() {
		return "Zone [id=" + id + ", naziv=" + naziv + ", cenaZaSat=" + cenaZaSat + ", dozvoljenoVremeParkingaUSatima="
				+ dozvoljenoVremeParkingaUSatima + "]";
	}
	
	
	
}
