package parking.model;

import java.util.Objects;

public class ParkingKarte {
	private long id;
	private String registracija;
	private String pocetakParkinga;
	private int trajanjeUMinutima;
	private String osobaSaInvaliditetom;
	private Zona zona;
	
	
	public ParkingKarte() {
	}


	public ParkingKarte(long id, String registracija, String pocetakParkinga, int trajanjeUMinutima,
			String osobaSaInvaliditetom, Zona zona) {
		this.id = id;
		this.registracija = registracija;
		this.pocetakParkinga = pocetakParkinga;
		this.trajanjeUMinutima = trajanjeUMinutima;
		this.osobaSaInvaliditetom = osobaSaInvaliditetom;
		this.zona = zona;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getRegistracija() {
		return registracija;
	}


	public void setRegistracija(String registracija) {
		this.registracija = registracija;
	}


	public String getPocetakParkinga() {
		return pocetakParkinga;
	}


	public void setPocetakParkinga(String pocetakParkinga) {
		this.pocetakParkinga = pocetakParkinga;
	}


	public int getTrajanjeUMinutima() {
		return trajanjeUMinutima;
	}


	public void setTrajanjeUMinutima(int trajanjeUMinutima) {
		this.trajanjeUMinutima = trajanjeUMinutima;
	}


	public String getOsobaSaInvaliditetom() {
		return osobaSaInvaliditetom;
	}


	public void setOsobaSaInvaliditetom(String osobaSaInvaliditetom) {
		this.osobaSaInvaliditetom = osobaSaInvaliditetom;
	}


	public Zona getZona() {
		return zona;
	}


	public void setZona(Zona zona) {
		this.zona = zona;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParkingKarte other = (ParkingKarte) obj;
		return id == other.id;
	}


	@Override
	public String toString() {
		return "parkingKarte [id=" + id + ", registracija=" + registracija + ", pocetakParkinga=" + pocetakParkinga
				+ ", trajanjeUMinutima=" + trajanjeUMinutima + ", osobaSaInvaliditetom=" + osobaSaInvaliditetom
				+ ", zona=" + zona + "]";
	}
	
	
}
