package parking.listeners;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;

import parking.controller.ParkingKarteController;
import parking.model.ParkingKarte;

@Component
public class InitHttpSessionListener implements HttpSessionListener{
	public void sessionCreated(HttpSessionEvent event) {
		System.out.println("Inicijalizacija sesisje HttpSessionListener...");
		
		List<ParkingKarte> parkingKarteList = new ArrayList<>();//zadatak4
		event.getSession().setAttribute(ParkingKarteController.KORISNIK_PARKING_KEY, parkingKarteList);
		
		System.out.println("Uspeh HttpSessionListener!");
	}
	
	public void sessionDestroyed(HttpSessionEvent event) {
		System.out.println("Brisanje sesisje HttpSessionListener...");
		
		System.out.println("Uspeh HttpSessionListener!");
	}	
}
