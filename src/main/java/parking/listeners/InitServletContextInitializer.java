package parking.listeners;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.stereotype.Component;

import parking.controller.ParkingKarteController;
import parking.model.Zona;
import parking.model.ParkingKarte;

@Component
public final class InitServletContextInitializer implements ServletContextInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
    	System.out.println("Inicijalizacija konteksta pri ServletContextInitializer...");
    	
    	Map<Integer, Zona> zonaMap = new LinkedHashMap<>();
    	zonaMap.put(1, new Zona(1, "bela", 50, 3));
    	zonaMap.put(2, new Zona(2, "plava", 100, 2));
    	zonaMap.put(3, new Zona(3, "crvena", 150, 1));
    	servletContext.setAttribute(ParkingKarteController.ZONA_KEY, zonaMap);
    	
    	Map<Long, ParkingKarte> parkingKarteMap = new LinkedHashMap<>();
    	parkingKarteMap.put(1L, new ParkingKarte(1, "SO 123 456", "2019-12-18 10:30:00", 130, "DA", zonaMap.get(3)));
    	parkingKarteMap.put(2L, new ParkingKarte(2, "NS 222 333", "2019-12-17 15:00:00", 45, "NE", zonaMap.get(1)));
    	parkingKarteMap.put(3L, new ParkingKarte(3, "BG 444 555", "2019-12-16 19:00:00", 90, "NE", zonaMap.get(2)));
    	parkingKarteMap.put(4L, new ParkingKarte(4, "NS 666 777", "2019-12-16 07:45:00", 20, "NE", zonaMap.get(3)));
    	parkingKarteMap.put(5L, new ParkingKarte(5, "SU 888 999", "2019-12-17 10:20:00", 150, "NE", zonaMap.get(3)));
    	parkingKarteMap.put(6L, new ParkingKarte(6, "BG 111 222", "2019-12-18 11:00:00", 150, "DA", zonaMap.get(2)));
    	servletContext.setAttribute(ParkingKarteController.PARKING_KARTE, parkingKarteMap);
    	
    	int kreiraneKarteZaCrvenuZonu = 0;
    	for (ParkingKarte it : parkingKarteMap.values()) {
			if(it.getZona().getNaziv().equals(ParkingKarteController.UKUPNO_KREIRANE_KARTE_ZONE_KEY)) {
				kreiraneKarteZaCrvenuZonu++;
			};
		}
    	servletContext.setAttribute(ParkingKarteController.UKUPNO_KREIRANE_KARTE_ZONE_KEY, kreiraneKarteZaCrvenuZonu);
    	
    	System.out.println("Uspeh ServletContextInitializer!");
    }

}
