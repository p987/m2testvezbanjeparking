package parking.dao;

import java.util.Collection;

import parking.model.Zona;

public interface ZonaDAO {
	
	public Collection<Zona> findAll();

	public void add(Zona zona);
}
 