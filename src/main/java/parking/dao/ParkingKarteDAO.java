package parking.dao;

import java.util.Collection;

import parking.model.ParkingKarte;

public interface ParkingKarteDAO {

	public Collection<ParkingKarte> findAll();

}
