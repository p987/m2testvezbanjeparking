package parking.dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import parking.model.Zona;

@Repository
public class ZonaDAOImpl implements parking.dao.ZonaDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	private static class ZonaRowMapper implements RowMapper<Zona> {

		@Override
		public Zona mapRow(ResultSet rs, int rowNum) throws SQLException {
			int i = 0;
			int zId = rs.getInt(++i);
			String zNaziv = rs.getString(++i);
			Double zCenaZaSat = rs.getDouble(++i);
			int zDozvoljenoVremeParkingaUSatima = rs.getInt(++i);
			
			Zona zona = new Zona(zId, zNaziv, zCenaZaSat, zDozvoljenoVremeParkingaUSatima);
			return zona;
		}
		
	}
	
	
	@Override
	public Collection<Zona> findAll() {
		String sql = "SELECT id, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima FROM zone;";
		return jdbcTemplate.query(sql, new ZonaRowMapper());
	}

	@Transactional
	@Override
	public void add(Zona zona) {
		PreparedStatementCreator psc = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO zone (naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima) VALUES (?, ?, ?);";
				PreparedStatement stmt = con.prepareStatement(sql);
				int i = 0;
				stmt.setString(++i, zona.getNaziv());
				stmt.setDouble(++i, zona.getCenaZaSat());
				stmt.setInt(++i, zona.getDozvoljenoVremeParkingaUSatima());
				return stmt;
			}
		};
		
		jdbcTemplate.update(psc);
	}

}
