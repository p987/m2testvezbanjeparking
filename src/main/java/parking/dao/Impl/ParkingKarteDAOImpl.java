package parking.dao.Impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import parking.dao.ParkingKarteDAO;
import parking.model.ParkingKarte;
import parking.model.Zona;

@Repository
public class ParkingKarteDAOImpl implements ParkingKarteDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static class ParkingKarteRowMapper implements RowMapper<ParkingKarte> {

		@Override
		public ParkingKarte mapRow(ResultSet rs, int rowNum) throws SQLException {
			int i = 0;
			long pId = rs.getLong(++i);
			String pRegistracija = rs.getString(++i);
			String pPocetakParkinga = rs.getString(++i);
			int pTrajanjeUMinutima = rs.getInt(++i);
			String pOsobaSaInvaliditetom = rs.getString(++i);
			
			int zId = rs.getInt(++i);
			String zNaziv = rs.getString(++i);
			Double zCenaZaSat = rs.getDouble(++i);
			int zDozvoljenoVremeParkingaUSatima = rs.getInt(++i);
			
			Zona zona = new Zona(zId, zNaziv, zCenaZaSat, zDozvoljenoVremeParkingaUSatima);
			ParkingKarte parkingKarte = new ParkingKarte(pId, pRegistracija, pPocetakParkinga, pTrajanjeUMinutima, pOsobaSaInvaliditetom, zona);
			return parkingKarte;
		}
		
	}
	
	@Override
	public Collection<ParkingKarte> findAll() {
		String sql = "SELECT p.id, p.registracija, p.pocetakParkinga, p.trajanjeUMinutima, p.osobaSaInvaliditetom, \r\n"
				+ "z.id, z.naziv, z.cenaZaSat, z.dozvoljenoVremeParkingaUSatima\r\n"
				+ "FROM parkingkarte p LEFT JOIN zone z ON p.zonaId = z.id ORDER BY p.id;";
		return jdbcTemplate.query(sql, new ParkingKarteRowMapper());
	}

}
